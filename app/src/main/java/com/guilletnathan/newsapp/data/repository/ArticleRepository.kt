package com.guilletnathan.newsapp.data.repository

import com.guilletnathan.newsapp.data.model.Article
import com.guilletnathan.newsapp.data.remote.NewsApi
import com.guilletnathan.newsapp.data.util.Constants.Companion.API_KEY
import javax.inject.Inject

class ArticleRepository @Inject constructor(
    private val newsApi: NewsApi,
) {

    suspend fun getAllArticles(searchQuery: String, pageNumber: Int) =
        newsApi.getNews(searchQuery, pageNumber, API_KEY)

}